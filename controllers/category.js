const Category = require("../models/category");


exports.getCategoryById = (req, res, next, id) => {

    Category.findById(id).exec((err, cate)=>{

        if(err)
        {
            return res.status(400).json({
                error :"category samruddh not found"
            });
        }
        req.category= cate;
        next();
    });
};

exports.createCategory = (req, res) => {

    const category = new Category(req.body) 

    category.save((err, category)=>{
        if(err)
        {
            return res.status(400).json({
                error :"Unable to save in db"
            });
        }
        res.json({ category });
    });


};

exports.getCategory = (req, res)=>{
    return res.jason(res.category);
};

exports.getAllCategory = (req, res)=>{
    Category.find().exec((err, categories)=>{
    if(err)
    {
        return res.status(400).json({
             error : "catedories not found in db"
        });
    }
    res.json(categories);
    });

};

exports.updateCategory = (req, res)=>
{
    const category = req.category;
    category.name = req.body.name;

    category.save((err, updateCategory)=>{
        if(err)
        {
            return res.status(400).json({
                error :"not able to update"
            });
        }
        return res.json(updateCategory);

    });
};


exports.removeCategory = (req, res)=>{
    const category = req.category

    category.remove((err, category)=>{
        if(err)
        {
            return res.status(400).json({
                error :"Fail to delete"
            });
        }
        res.json({
            Message : "Remove sucessfully"
        });
    });



};